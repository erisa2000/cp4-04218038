Rumus=input("Pilih rumus luas (segitiga/persegi panjang/trapesium) : ")
Rumus=Rumus.lower()
if (Rumus=="segitiga"):
    A=input("Masukkan alas segitiga : ")
    B=input("Masukkan tinggi segitiga : ")
    print("Hasil dari luas segitiga adalah : ", int(A)*int(B)/2)
elif(Rumus=="persegi panjang"):
    A=input("Masukkan panjang persegi panjang : ")
    B=input("Masukkan lebar persegi panjang : ")
    print("Hasil dari luas persegi panjang adalah : ", int(A)*int(B))
elif(Rumus=="trapesium"):
    A=input("Masukkan bagian atas trapesium : ")
    B=input("Masukkan bagian bawah trapesium : ")
    C=input("Masukkan tinggi trapesium :")
    print("Hasil dari luas trapesium adalah : ", (int(A)+int(B))*int(C)/2)
else:
    print(Rumus, "Rumus belum tersedia |||")

print("Terima Kasih")
print()
